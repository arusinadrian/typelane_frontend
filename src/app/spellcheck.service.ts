import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SpellcheckService {
  configUrl = 'assets/config.json';

  constructor(private http: HttpClient) { }

  fetchMisspelledWords(text: string) {
    return this.http.post('http://localhost:3000/spellcheck', {
      text,
    });
  }

  fetchFixedText(id: string) {
    return this.http.get(`http://localhost:3000/spellcheck/${id}`);
  }
}
