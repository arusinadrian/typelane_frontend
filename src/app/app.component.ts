import { Component, Inject, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SpellcheckService } from './spellcheck.service';

interface DialogData {}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  form: FormGroup;
  title = 'typelane-front';
  didFix: boolean;
  fixedSentence = '';

  constructor(
    public fb: FormBuilder,
    public dialog: MatDialog,
    private readonly spellcheckService: SpellcheckService,
  ) {
    this.form = this.fb.group({
      inputText: ['', Validators.required],
      floatLabel: 'auto',
    });
  }

  submit(value) {
    this.spellcheckService.fetchMisspelledWords(value.inputText).subscribe((data: any) => {
      const { misspelledWords, id } = data;
      const dialogRef = this.dialog.open(DialogQuestionComponent, {
        width: '250px',
        data: {
          typosList: misspelledWords,
          sentence: value.inputText,
        },
      });

      const shouldFix = dialogRef.componentInstance.shouldFix.subscribe(() => {
        this.didFix = true;
        this.spellcheckService.fetchFixedText(id).subscribe((data: any) => {
          const { text } = data;
          console.log(text);

          this.fixedSentence = text;
        });
      });

      dialogRef.afterClosed().subscribe(result => {
        shouldFix.unsubscribe();
      });
    });

  }
}

@Component({
  selector: 'app-dialog-question',
  templateUrl: './dialog-example.html',
})
export class DialogQuestionComponent {
  shouldFix = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<DialogQuestionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.shouldFix.emit('fix');
    this.dialogRef.close();
  }
}
